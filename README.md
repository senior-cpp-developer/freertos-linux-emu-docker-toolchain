# FreeRTOS Linux Emulator Docker Toolchain

## Building the container
1. Clone the repo
```bash
git clone https://gitlab.com/senior-cpp-developer/freertos-linux-emu-docker-toolchain.git
```
2. Build the container
```bash
docker build freertos-linux-emu-docker-toolchain/
```

## Connect to CLion
Check [this tutorial.](https://www.jetbrains.com/help/clion/clion-toolchains-in-docker.html#create-docker-toolchain)
