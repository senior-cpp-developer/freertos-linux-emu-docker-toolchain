FROM ubuntu:20.04

RUN DEBIAN_FRONTEND="noninteractive" apt-get update && apt-get -y install tzdata

RUN apt-get update \
  && apt-get install -y build-essential \
      gcc \
      g++ \
      gdb \
      clang \
      make \
      ninja-build \
      cmake \
      autoconf \
      automake \
      locales-all \
      dos2unix \
      rsync \
      tar \
      python \
      python-dev \
      \
      libpcap-dev \
      git \
  && apt-get clean

RUN mkdir /toolchain
WORKDIR /toolchain
RUN git clone --recurse-submodules https://github.com/FreeRTOS/FreeRTOS.git

WORKDIR /toolchain/FreeRTOS/FreeRTOS/Demo/Posix_GCC/
RUN make
